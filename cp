#!/bin/sh
. git-shell-commands/functions.sh
checkperms || exit $?
if [ $# -ne 2 ]; then
  echo "Usage: $(basename $0) source target"
  exit 1
fi
execute "cp -a "$(gitname "$1")" "$(gitname "$2")""

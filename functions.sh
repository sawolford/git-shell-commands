basedir=$(realpath $(dirname $(dirname $0)))

checkpath() {
    folder="$1"
    if [[ "$folder" == "/"* || "$folder" == ".."* ]]; then
        echo Path \"$folder\" disallowed
        exit 1
    fi
    echo "$folder"
}

execute() {
    echo "$@"
    eval "$@"
    return $?
}

datestring() {
    date "+%F-%T"
}

gitname() {
    echo "${1/.git/}.git"
}

checkperms() {
    . ~/git-shell-commands/permissions.sh
    bcmd=$(basename $0)
    if ! check$bcmd; then
        echo Permission denied
        return 1
    fi
    return 0
}
